package eps.ua.es.filmoteca;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.drive.query.SortOrder;
import com.google.android.gms.drive.query.SortableField;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class FilmDataSource extends Activity
{
    private static List<Film> films;
    private static final String NOMBRE_FICHERO_DATOS = "datospeliculas.dat";

    private static DriveFile m_drive_file;
    private static Context context_activity;

    // --------------------------------------------------------------

    public FilmDataSource(Context context)
    {
        this.context_activity = context;
    }

    // --------------------------------------------------------------

    public FilmDataSource()
    {

    }

    // --------------------------------------------------------------

    private static void i_creaPeliculasPorDefecto()
    {
        Film film;

        films = new ArrayList<Film>();

        film = new Film();
        film.title = "Regreso al futuro";
        film.director = "Robert Zemeckis";
        film.latitude = 37.0902405;
        film.longitude = -95.7128906;
        film.imageResId = R.mipmap.ic_launcher;
        film.comments = "";
        film.format = Film.FORMAT_DIGITAL;
        film.genre = Film.GENRE_SCIFI;
        film.imdbUrl = "http://www.imdb.com/title/tt0088763";
        film.year = 1985;
        films.add(film);

        film = new Film();
        film.title = "Aida's Secrets";
        film.director = "Alon Schwarz, Shaul Schwarz";
        film.latitude = 51.0000000;
        film.longitude = 9.0000000;
        film.imageResId = R.mipmap.ic_launcher;
        film.comments = "";
        film.format = Film.FORMAT_DIGITAL;
        film.genre = Film.GENRE_DRAMA;
        film.imdbUrl = "http://www.imdb.com/title/tt5706568";
        film.year = 2016;
        films.add(film);

        film = new Film();
        film.title = "Acrimony";
        film.director = "Tyler Perry";
        film.longitude = 65.0000000;
        film.latitude = 33.0000000;
        film.imageResId = R.mipmap.ic_launcher;
        film.comments = "";
        film.format = Film.FORMAT_DVD;
        film.genre = Film.GENRE_DRAMA;
        film.imdbUrl = "http://www.imdb.com/title/tt6063050";
        film.year = 2018;
        films.add(film);

        film = new Film();
        film.title = "12 Strong";
        film.director = "Nicolai Fuglsig";
        film.longitude = 1.6000000;
        film.latitude = 42.50000000;
        film.imageResId = R.mipmap.ic_launcher;
        film.comments = "12 Strong tells the story of the first Special Forces team deployed to Afghanistan after 9/11; under the leadership of a new captain, the team must work with an Afghan warlord to take down the Taliban.";
        film.format = Film.FORMAT_DIGITAL;
        film.genre = Film.GENRE_ACTION;
        film.imdbUrl = "http://www.imdb.com/title/tt1413492";
        film.year = 2018;
        films.add(film);

        film = new Film();
        film.title = "Miss You Already";
        film.director = "Catherine Hardwicke";
        film.longitude = -76.000000;
        film.latitude = 24.2500000;
        film.imageResId = R.mipmap.ic_launcher;
        film.comments = "";
        film.format = Film.FORMAT_BLURAY;
        film.genre = Film.GENRE_COMEDY;
        film.imdbUrl = "http://www.imdb.com/title/tt2245003";
        film.year = 2015;
        films.add(film);

        film = new Film();
        film.title = "Saw";
        film.director = "James Wan";
        film.longitude = 90.000000;
        film.latitude = 24.0000000;
        film.imageResId = R.mipmap.ic_launcher;
        film.comments = "Two strangers awaken in a room with no recollection of how they got there or why, and soon discover they are pawns in a deadly game perpetrated by a notorious serial killer.";
        film.format = Film.FORMAT_DVD;
        film.genre = Film.GENRE_HORROR;
        film.imdbUrl = "http://www.imdb.com/title/tt0387564";
        film.year = 2004;
        films.add(film);
    }

    // --------------------------------------------------------------

    private DriveResourceClient getDriveResourceClient(Context context)
    {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(context_activity);

        // Get the app's Drive folder
        DriveResourceClient client = Drive.getDriveResourceClient(context_activity, account);

        return client;
    }

    // --------------------------------------------------------------

    private void i_crearFicheroDrive(final Context context, final boolean con_datos_defecto)
    {
        // [START create_file]
        final Task<DriveFolder> appFolderTask = getDriveResourceClient(context_activity).getAppFolder();
        final Task<DriveContents> createContentsTask = getDriveResourceClient(context_activity).createContents();

        Tasks.whenAll(appFolderTask, createContentsTask).continueWithTask(new Continuation<Void, Task<DriveFile>>()
        {
            @Override
            public Task<DriveFile> then(@NonNull Task<Void> task) throws Exception
            {
                DriveFolder parent = appFolderTask.getResult();
                DriveContents contents = createContentsTask.getResult();
                OutputStream outputStream = contents.getOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(outputStream);
                    /*
                    try (Writer writer = new OutputStreamWriter(outputStream)) {
                        writer.write("Archivo de peliculas");
                    }*/

                if (con_datos_defecto == true)
                {
                    i_creaPeliculasPorDefecto();
                }

                oos.writeObject(films);

                MetadataChangeSet changeSet = new MetadataChangeSet.Builder().setTitle(NOMBRE_FICHERO_DATOS).setMimeType("text/plain").setStarred(true).build();

                return getDriveResourceClient(context_activity).createFile(parent, changeSet, contents);
            }
        }).addOnSuccessListener((Activity) context_activity, new OnSuccessListener<DriveFile>()
        {
            @Override
            public void onSuccess(DriveFile driveFile)
            {
                //showMessage(getString(R.string.file_created, driveFile.getDriveId().encodeToString());
                Log.d("GOOGLE_DRIVE", "Create file");
                System.out.println("GOOGLE_DRIVE Create file");

                m_drive_file = driveFile;

                i_establecerAdaptadorListFragment(context_activity);

                finish();
            }
        }).addOnFailureListener((Activity) context_activity, new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception e)
            {
                Log.d("GOOGLE_DRIVE", "Unable to create file", e);
                //showMessage(getString(R.string.file_create_error));
                finish();
            }
        });

        // [END create_file]
    }

    // --------------------------------------------------------------

    private void i_establecerAdaptadorListFragment(final Context context)
    {
        FilmListFragment.adapter = new FilmsArrayAdapter(context_activity, R.layout.item_film, films);

        FilmListFragment.list_view.setAdapter(FilmListFragment.adapter);
    }

    // --------------------------------------------------------------

    private void i_leerArchivoGoogleDrive(final Context context, DriveId drive_id)
    {
        Log.e("GOOGLE_DRIVE", "Entra en leer");
        DriveFile file = drive_id.asDriveFile();

        m_drive_file = file;

        Task<DriveContents> openFileTask =
                getDriveResourceClient(context_activity).openFile(file, DriveFile.MODE_READ_ONLY);

        openFileTask
                .continueWithTask(new Continuation<DriveContents, Task<Void>>()
                {
                    @Override
                    public Task<Void> then(@NonNull Task<DriveContents> task) throws Exception {
                        DriveContents contents = task.getResult();
                        // Process contents...
                        // ...
                        Log.d("GOOGLE_DRIVE", "Continua con leer");

                        ObjectInputStream ois;

                        InputStream inputStream = contents.getInputStream();

                        ois = new ObjectInputStream(inputStream);

                        films = (ArrayList<Film>) ois.readObject();
                        System.out.println(films.size());
                        /*
                        List<Film> films_prueba = (ArrayList<Film>) ois.readObject();

                        for (int i = 0; i < films_prueba.size(); i++)
                        {
                            Film film = films_prueba.get(i);
                            System.out.println(film.getTitle());
                            System.out.println(film.getDirector());
                            System.out.println(film.imageResId);
                            System.out.println(film.comments);
                            System.out.println(film.format);
                            System.out.println(film.getGenre());
                            System.out.println(film.getImdbUrl());
                            System.out.println(film.getYear());
                        }
                        */

                        inputStream.close();
                        ois.close();

                        Task<Void> discardTask = getDriveResourceClient(context_activity).discardContents(contents);

                        i_establecerAdaptadorListFragment(context_activity);

                        return discardTask;
                    }
                })
                .addOnFailureListener(new OnFailureListener()
                {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Handle failure
                        // ...
                        Log.e("GOOGLE_DRIVE", "Fallo abriendo el archivo");
                    }
                });
    }

    // --------------------------------------------------------------

    public void i_leeCreaArchivoGoogleDrive(final Context context)
    {
        SortOrder sortOrder = new SortOrder.Builder().addSortAscending(SortableField.MODIFIED_DATE).build();
        Query query = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, NOMBRE_FICHERO_DATOS))
                .setSortOrder(sortOrder)
                .build();

        Task<MetadataBuffer> queryTask = getDriveResourceClient(context_activity).query(query);

        queryTask
                .addOnSuccessListener((Activity) context_activity,
                        new OnSuccessListener<MetadataBuffer>()
                        {
                            @Override
                            public void onSuccess(MetadataBuffer metadataBuffer)
                            {
                                // Handle results...
                                // ...
                                if (metadataBuffer.getCount() > 0)
                                {
                                    Log.d("GOOGLE_DRIVE", metadataBuffer.get(0).getTitle());
                                    System.out.println("GOOGLE_DRIVE " + metadataBuffer.get(0).getTitle());

                                    DriveId drive_id_file = metadataBuffer.get(0).getDriveId();

                                    metadataBuffer.release();

                                    i_leerArchivoGoogleDrive(context_activity, drive_id_file);
                                }
                                else
                                {
                                    Log.d("GOOGLE_DRIVE", "No se ha encontrado el archivo");
                                    System.out.println("GOOGLE_DRIVE No se ha encontrado el archivo");

                                    metadataBuffer.release();

                                    i_crearFicheroDrive(context_activity, true);
                                }
                            }


                        })
                .addOnFailureListener((Activity) context_activity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e)
                    {
                        // Handle failure...
                        // ...
                        Log.d("GOOGLE_DRIVE", "No encontrado file");
                        System.out.println("GOOGLE_DRIVE Error durante la busqueda");

                    }
                });
    }

    // --------------------------------------------------------------

    public static void guardarFicheroDatosPeliculas(Context context)
    {
        FileOutputStream fos;
        ObjectOutputStream oos;

        fos = null;
        oos = null;

        try
        {
            File directorio;
            File archivo_datos_peliculas;

            directorio = context_activity.getFilesDir();
            archivo_datos_peliculas = new File(directorio, NOMBRE_FICHERO_DATOS);

            fos = new FileOutputStream(archivo_datos_peliculas);
            oos = new ObjectOutputStream(fos);

            oos.writeObject(films);

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                fos.close();
                oos.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    // --------------------------------------------------------------

    private static void i_leerFicheroDatosPeliculas(Context context)
    {
        FileInputStream fis;
        ObjectInputStream ois;

        fis = null;
        ois = null;

        try
        {
            File directorio;
            File archivo_datos_peliculas;

            directorio = context_activity.getFilesDir();
            archivo_datos_peliculas = new File(directorio, NOMBRE_FICHERO_DATOS);

            fis = new FileInputStream(archivo_datos_peliculas);
            ois = new ObjectInputStream(fis);

            films = (ArrayList<Film>) ois.readObject();

            ois.close();
            fis.close();

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (ois != null)
            {
                try
                {
                    ois.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }

            if (fis != null)
            {
                try
                {
                    fis.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }

        }
    }

    // --------------------------------------------------------------

    public static boolean existeFicheroDatosPeliculas(Context context)
    {
        File directorio;
        File archivo_datos_peliculas;
        FileInputStream fis;

        directorio = context_activity.getFilesDir();

        archivo_datos_peliculas = new File(directorio, NOMBRE_FICHERO_DATOS);

        if (archivo_datos_peliculas.exists() == true && archivo_datos_peliculas.isDirectory() != false)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // --------------------------------------------------------------

    public void getFilms(Context context)
    {
        i_leeCreaArchivoGoogleDrive(context_activity);
    }

    // --------------------------------------------------------------

    public void actualizarFicheroDrive(final Context context)
    {
        getDriveResourceClient(context_activity)
                .delete(m_drive_file)
                .addOnSuccessListener((Activity) context_activity,
                        new OnSuccessListener<Void>()
                        {
                            @Override
                            public void onSuccess(Void aVoid) {
                                //showMessage(getString(R.string.file_deleted));

                                Log.d("GOOGLE_DRIVE", "Fichero borrado");
                                System.out.println("GOOGLE_DRIVE Fichero borrado");

                                i_crearFicheroDrive(context_activity, false);

                                finish();
                            }
                        })
                .addOnFailureListener((Activity) context_activity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e)
                    {
                        /*
                        Log.e(TAG, "Unable to delete file", e);
                        showMessage(getString(R.string.delete_failed));
                        */

                        Log.e("GOOGLE_DRIVE", "Fichero no borrado");
                        System.out.println("GOOGLE_DRIVE Fichero no borrado");

                        finish();
                    }
                });
    }

    // --------------------------------------------------------------

    public static Film getFilm(int posicion)
    {
        return films.get(posicion);
    }

    // --------------------------------------------------------------

    public void anadir_nuevo_film(Film f, Context context)
    {
        films.add(f);
        actualizarFicheroDrive(context_activity);
    }

    // --------------------------------------------------------------

    public void borrarListaFilms(List<Film> peliculas_para_borrar, Context context)
    {
        films.removeAll(peliculas_para_borrar);
        guardarFicheroDatosPeliculas(context_activity);

        actualizarFicheroDrive(context_activity);
    }

    // --------------------------------------------------------------

    public static boolean hayFilms()
    {
        if (films.size() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
