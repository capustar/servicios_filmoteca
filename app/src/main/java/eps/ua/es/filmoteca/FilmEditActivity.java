package eps.ua.es.filmoteca;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import static eps.ua.es.filmoteca.FilmDataSource.guardarFicheroDatosPeliculas;

public class FilmEditActivity extends AppCompatActivity
{
    EditText edit_text_titulo_pelicula;
    EditText edit_text_nombre_director;
    EditText edit_text_ano_pelicula;
    EditText edit_text_latitud_pelicula;
    EditText edit_text_longitud_pelicula;
    EditText edit_text_enlace_imdb;
    Spinner spinner_genero;
    Spinner spinner_formato;
    EditText edit_text_text_comentarios;

    private Film i_FilmSeleccionado()
    {
        Intent intent;
        int indice_film_seleccionado;

        intent = getIntent();
        indice_film_seleccionado = intent.getIntExtra(MainActivity.POSICION_FILM_SELECCIONADO_EXTRA, 0);

        return FilmDataSource.getFilm(indice_film_seleccionado);
    }

    // --------------------------------------------------------------

    private void i_setDatosFilm(Film film)
    {
        FilmDataSource film_data_source;

        assert(film != null);

        film.setTitle(edit_text_titulo_pelicula.getText().toString());

        film.setDirector(edit_text_nombre_director.getText().toString());

        film.setYear(Integer.parseInt(edit_text_ano_pelicula.getText().toString()));

        film.setLatitude(Double.parseDouble(edit_text_latitud_pelicula.getText().toString()));
        film.setLongitude(Double.parseDouble(edit_text_longitud_pelicula.getText().toString()));

        film.setImdbUrl(edit_text_enlace_imdb.getText().toString());

        film.setGenre(spinner_genero.getSelectedItemPosition());

        film.setFormat(spinner_formato.getSelectedItemPosition());

        film.setComments(edit_text_text_comentarios.getText().toString());

        film_data_source = new FilmDataSource();
        film_data_source.actualizarFicheroDrive(this);
    }

    // --------------------------------------------------------------

    private void i_guardarCambios(Button button, final Film film)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {

                i_setDatosFilm(film);

                setResult(RESULT_OK, null);
                finish();
            }
        });
    }

    // --------------------------------------------------------------

    private void i_cancelarCambios(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                setResult(RESULT_CANCELED, null);
                finish();
            }
        });
    }

    // --------------------------------------------------------------

    private void i_inicializaDatosVista(final Film film)
    {
        assert(film != null);

        edit_text_titulo_pelicula.setText(film.getTitle());

        edit_text_nombre_director.setText(film.getDirector());

        edit_text_ano_pelicula.setText(String.valueOf(film.getYear()));

        edit_text_latitud_pelicula.setText(String.valueOf(film.getLatitude()));
        edit_text_longitud_pelicula.setText(String.valueOf(film.getLongitude()));

        edit_text_enlace_imdb.setText(film.getImdbUrl());

        spinner_genero.setSelection(film.getGenre());

        spinner_formato.setSelection(film.getFormat());

        edit_text_text_comentarios.setText(film.getComments());
    }

    // --------------------------------------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);

        int id_item;

        id_item = item.getItemId();

        if (id_item == android.R.id.home)
        {
            Intent intent;

            intent = new Intent(this, FilmListFragment.class);
            NavUtils.navigateUpTo(this, intent);

            return true;
        }
        else
        {
            return false;
        }

    }

    // --------------------------------------------------------------

    private void i_mostrarIconoAplicacion()
    {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // --------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_edit);
        i_mostrarIconoAplicacion();

        Film film;
        Button button_guardar;
        Button button_cancelar;

        film = i_FilmSeleccionado();

        edit_text_titulo_pelicula = (EditText)findViewById(R.id.editTextTituloPelicula);
        edit_text_nombre_director = (EditText)findViewById(R.id.editTextNombreDirector);
        edit_text_ano_pelicula = (EditText)findViewById(R.id.editTextAnoPelicula);
        edit_text_latitud_pelicula = (EditText)findViewById(R.id.editTextLatitudPelicula);
        edit_text_longitud_pelicula = (EditText)findViewById(R.id.editTextLongitudPelicula);
        edit_text_enlace_imdb = (EditText)findViewById(R.id.editTextEnlaceIBDM);
        spinner_genero = (Spinner)findViewById(R.id.spinnerGenero);
        spinner_formato = (Spinner)findViewById(R.id.spinnerFormato);
        edit_text_text_comentarios = (EditText)findViewById(R.id.editTextComentarios);

        i_inicializaDatosVista(film);

        button_guardar = (Button) findViewById(R.id.buttonGuardar);
        i_guardarCambios(button_guardar, film);

        button_cancelar  = (Button) findViewById(R.id.buttonCancelar);
        i_cancelarCambios(button_cancelar);
    }
}
