package eps.ua.es.filmoteca;

import android.app.Application;
import android.content.Context;

/**
 * Created by RosaDiez on 06/04/2018.
 */

public class GlobalClass extends Application
{
    public static Context context;

    @Override
    public void onCreate()
    {
        super.onCreate();
        context = getApplicationContext();
    }
}
