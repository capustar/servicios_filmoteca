package eps.ua.es.filmoteca;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.*;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

public class FilmListFragment extends ListFragment
{
    static final private int ID_MENU_ANADIR_PELICULA = Menu.FIRST;
    static final private int ID_MENU_ACERCA_DE = Menu.FIRST + 1;
    static final private int ID_MENU_CERRAR_SESION = Menu.FIRST + 2;
    static final private int ID_MENU_DESCONECTAR_APLICACION = Menu.FIRST + 3;

    private final int i_ACTIVIDAD_SIGN_IN_TWITTER = 1;

    public static ListView list_view;
    public static ArrayAdapter<Film> adapter;

    FilmDataSource m_film_data_source;

    OnItemSelectedListener mCallback;
    GoogleSignInClient m_google_sign_in_client;

    // --------------------------------------------------------------

    private void i_configuracion_seleccion_multiple()
    {
        final ListView lista;

        lista = getListView();

        lista.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        lista.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener()
        {

            // --------------------------------------------------------------

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
            {
                MenuInflater inflater = getActivity().getMenuInflater();
                inflater.inflate(R.menu.menu_borrar, menu);
                return true;
            }

            // --------------------------------------------------------------

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu)
            {
                return false;
            }

            // --------------------------------------------------------------

            private void i_borrarPeliculasSeleccionadas()
            {
                SparseBooleanArray peliculas_seleccionadas;
                int i;
                List<Film> peliculas_para_borrar;

                peliculas_seleccionadas = lista.getCheckedItemPositions();

                peliculas_para_borrar = new ArrayList<Film>();

                if (peliculas_seleccionadas != null)
                {
                    Toast toast;

                    for (i = 0; i < peliculas_seleccionadas.size(); i++)
                    {
                        if (peliculas_seleccionadas.valueAt(i) == true)
                        {
                            Film pelicula_seleccionada;

                            pelicula_seleccionada = FilmDataSource.getFilm(peliculas_seleccionadas.keyAt(i));
                            peliculas_para_borrar.add(pelicula_seleccionada);
                        }
                    }

                    m_film_data_source.borrarListaFilms(peliculas_para_borrar, getContext());

                    toast = Toast.makeText(getContext(), "Se han borrado las peliculas.", Toast.LENGTH_SHORT);

                    toast.show();
                }

                adapter.notifyDataSetChanged();
            }

            // --------------------------------------------------------------

            private void i_enviarTweet(List<Film> peliculas_seleccionadas_favoritas)
            {
                Twitter twitter;
                boolean esta_logueado;

                twitter = new Twitter(getContext());

                esta_logueado = twitter.estaLogueado();

                if (esta_logueado == true)
                {
                    //twitter.getUser();
                    //Aqui enviaria el tweet

                    int numero_peliculas, i;

                    numero_peliculas = peliculas_seleccionadas_favoritas.size();

                    for (i = 0; i < numero_peliculas; i++)
                    {
                        Film film;
                        String titulo_pelicula;

                        film = peliculas_seleccionadas_favoritas.get(i);

                        titulo_pelicula = film.getTitle();
                        twitter.sendTweet(titulo_pelicula);
                    }
                }
                else
                {
                    String auth_url;
                    Toast toast;

                    auth_url = twitter.getAuthUrl();

                    if (auth_url != null)
                    {
                        Intent intent_sign_in_twitter;

                        intent_sign_in_twitter = new Intent(Intent.ACTION_VIEW, Uri.parse(auth_url));

                        toast = Toast.makeText(GlobalClass.context, "Debes iniciar sesión antes.", Toast.LENGTH_SHORT);

                        toast.show();

                        Log.d(twitter.TAG, "Solcitud de autorización: Una vez obtenidas las claves temporales, manda a que el usuario autorize el acceso");
                        startActivity(intent_sign_in_twitter);
                    }
                    else
                    {
                        Log.d(twitter.TAG, "Solcitud de autorización: Ha habido un problema obteniendo las claves temporales");
                    }
                }
            }

            // --------------------------------------------------------------

            private void i_anadirFavoritas()
            {
                SparseBooleanArray peliculas_seleccionadas;
                int i;
                List<Film> peliculas_seleccionadas_favoritas;

                peliculas_seleccionadas = lista.getCheckedItemPositions();

                peliculas_seleccionadas_favoritas = new ArrayList<Film>();

                if (peliculas_seleccionadas != null)
                {
                    for (i = 0; i < peliculas_seleccionadas.size(); i++)
                    {
                        if (peliculas_seleccionadas.valueAt(i) == true)
                        {
                            Film pelicula_seleccionada;

                            pelicula_seleccionada = FilmDataSource.getFilm(peliculas_seleccionadas.keyAt(i));
                            peliculas_seleccionadas_favoritas.add(pelicula_seleccionada);
                        }
                    }

                    i_enviarTweet(peliculas_seleccionadas_favoritas);
                }
            }

            // --------------------------------------------------------------

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.menuBorrar:

                        i_borrarPeliculasSeleccionadas();
                        actionMode.finish();

                        return true;

                    case R.id.menuFavorita:

                        i_anadirFavoritas();
                        actionMode.finish();

                        return true;

                    default:
                        return false;
                }
            }

            // --------------------------------------------------------------

            @Override
            public void onDestroyActionMode(ActionMode actionMode)
            {

            }

            // --------------------------------------------------------------

            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b)
            {
                int num_items_seleccionados;
                String texto;

                num_items_seleccionados = lista.getCheckedItemCount();
                texto = "seleccionados";
                actionMode.setTitle(num_items_seleccionados + " " + texto);
            }
        });
    }

    // --------------------------------------------------------------

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        //new DownloadTask().execute();

        list_view = getListView();

        m_film_data_source = new FilmDataSource(getActivity());

        m_film_data_source.getFilms(getContext());

        //adapter = new FilmsArrayAdapter(getContext(), R.layout.item_film, m_films);
        //setListAdapter(adapter);
        i_configuracion_seleccion_multiple();
    }

    // --------------------------------------------------------------

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        mCallback.onItemSelected(position);
    }

    // --------------------------------------------------------------

    private void i_abrir_acerca_de()
    {
        Intent intent;

        intent = new Intent(getContext(), AboutActivity.class);
        startActivity(intent);
    }

    // --------------------------------------------------------------

    private void i_nueva_pelicula()
    {
        Film film;
        Toast toast;

        film = new Film();
        film.title = "Nueva pelicula";
        film.director = "Desconocido";
        film.imageResId = R.mipmap.ic_launcher;
        m_film_data_source.anadir_nuevo_film(film, getContext());

        toast = Toast.makeText(getContext(), "Se ha anadido una pelicula nueva", Toast.LENGTH_SHORT);

        toast.show();

        adapter.notifyDataSetChanged();
    }

    // --------------------------------------------------------------

    private void i_abrirActividadInicio()
    {
        Intent intent;
        intent = new Intent(getActivity(), InicioActivity.class);

        // set the new task and clear flags
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    // --------------------------------------------------------------

    private void i_cerrarSesion()
    {
        m_google_sign_in_client.signOut()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        Twitter twitter;

                        twitter = new Twitter(getContext());
                        twitter.cerrarSesion();

                        i_abrirActividadInicio();
                    }
                });
    }

    // --------------------------------------------------------------

    private void i_desconectarAplicacion()
    {
        m_google_sign_in_client.revokeAccess()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        Twitter twitter;

                        twitter = new Twitter(getContext());
                        twitter.cerrarSesion();

                        i_abrirActividadInicio();
                    }
                });
    }

    // --------------------------------------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);

        switch (item.getItemId())
        {
            case R.id.menuAcercaDe:

                i_abrir_acerca_de();
                return true;

            case R.id.menuNuevaPelicula:

                i_nueva_pelicula();
                return true;

            case R.id.menuCerrarSesion:

                i_cerrarSesion();
                return true;

            case R.id.menuDesconectarAplicacion:

                i_desconectarAplicacion();
                return true;

            default:
                return false;
        }
    }

    // --------------------------------------------------------------

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.main_list_menu_actions, menu);
    }

    // --------------------------------------------------------------

    public interface OnItemSelectedListener
    {
        public void onItemSelected(int position);
    }

    // --------------------------------------------------------------

    private void i_obtenerUsuario()
    {
        GoogleSignInOptions google_sign_in_options;

        google_sign_in_options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        m_google_sign_in_client = GoogleSignIn.getClient(getActivity(), google_sign_in_options);
    }

    // --------------------------------------------------------------

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // REVISAR ESTO YA QUE ES UNA SOLUCION MAS ELEGANTE
        //Log.d(Twitter.TAG, "Entra aqui");
        if (requestCode == i_ACTIVIDAD_SIGN_IN_TWITTER && data != null)
        {
            /*
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
            */

            //Log.d(Twitter.TAG, "Entra aqui");

            final Uri uri = data.getData();

            if (uri != null/* && uri.toString().startsWith(Twitter.CALLBACKURL)*/)
            {
                Log.d(Twitter.TAG, "onResume: El usuario ha respondido y vuelve a invocar a la aplicación");

                String verifier = uri.getQueryParameter("oauth_verifier");
                if (verifier != null)
                {
                    Twitter twitter;
                    Log.d(Twitter.TAG, "onResume: El usuario ha autorizado el acceso en su nombre y se nos devuelve el verificador");

                    twitter = new Twitter(getContext());
                    // 2
                    //new OauthEnd().execute(verifier);
                    twitter.oauthEnd(verifier);

                    // 3
                    //new getUser().execute();
                    twitter.getUser();
                }
                else
                {
                    Log.d(Twitter.TAG, "onResume: El usuario NO ha autorizado el acceso en su nombre");
                }
            }

        }
    }

    // --------------------------------------------------------------

    @Override
    public void onResume()
    {
        super.onResume();

        Log.d(Twitter.TAG, "Entra aqui");

        final Uri uri = this.getActivity().getIntent().getData();

        if (uri != null/* && uri.toString().startsWith(Twitter.CALLBACKURL)*/)
        {
            Log.d(Twitter.TAG, "onResume: El usuario ha respondido y vuelve a invocar a la aplicación");

            String verifier = uri.getQueryParameter("oauth_verifier");
            if (verifier != null)
            {
                Twitter twitter;
                Log.d(Twitter.TAG, "onResume: El usuario ha autorizado el acceso en su nombre y se nos devuelve el verificador");

                twitter = new Twitter(getContext());
                // 2
                //new OauthEnd().execute(verifier);
                twitter.oauthEnd(verifier);

                // 3
                //new getUser().execute();
                twitter.getUser();
            }
            else
            {
                Log.d(Twitter.TAG, "onResume: El usuario NO ha autorizado el acceso en su nombre");
            }
        }
    }


    // --------------------------------------------------------------

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        i_obtenerUsuario();
    }

    // --------------------------------------------------------------

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        try
        {
            mCallback = (OnItemSelectedListener) context;
        }
        catch (ClassCastException e)
        {

        }
    }

    // --------------------------------------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View vista;

        vista = inflater.inflate(R.layout.activity_film_list, container, false);

        return vista;
    }
}

