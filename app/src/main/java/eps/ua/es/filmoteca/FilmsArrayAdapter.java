package eps.ua.es.filmoteca;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class FilmsArrayAdapter extends ArrayAdapter<Film>
{
    public FilmsArrayAdapter(Context context, int resource, List<Film> objects)
    {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = LayoutInflater.from(this.getContext()).
                    inflate(R.layout.item_film, parent, false);
        }

        Film film_seleccionado;
        ImageView image_icono;
        TextView text_view_titulo;
        TextView text_view_director;

        film_seleccionado = getItem(position);

        image_icono = (ImageView)convertView.findViewById(R.id.imageIcono);
        image_icono.setImageResource(film_seleccionado.imageResId);

        text_view_titulo = (TextView)convertView.findViewById(R.id.textViewTitulo);
        text_view_titulo.setText(film_seleccionado.title);

        text_view_director = (TextView)convertView.findViewById(R.id.textViewDirector);
        text_view_director.setText(film_seleccionado.director);

        return convertView;
    }
}
