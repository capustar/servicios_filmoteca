package eps.ua.es.filmoteca;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by RosaDiez on 04/04/2018.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService
{

    // --------------------------------------------------------------

    private void sendRegistrationToServer(String token)
    {
        // TODO: Implement this method to send token to your app server.
    }

    // --------------------------------------------------------------

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("FirebaseInstanceID", "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
}
