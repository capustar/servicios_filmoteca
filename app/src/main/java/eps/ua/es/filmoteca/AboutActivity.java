package eps.ua.es.filmoteca;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

public class AboutActivity extends AppCompatActivity
{
    // --------------------------------------------------------------

    private void lanzarPaginaWeb(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent_mostrar_web;

                intent_mostrar_web = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.ua.es"));
                startActivity(intent_mostrar_web);
            }
        });
    }

    // --------------------------------------------------------------

    private void enviarEmail(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent_mostrar_web;

                intent_mostrar_web = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:mvmt94@gmail.com"));
                startActivity(intent_mostrar_web);
            }
        });
    }

    // --------------------------------------------------------------

    private void finalizarAboutActivity(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                finish();
            }
        });
    }

    // --------------------------------------------------------------

    private void i_obtenerDatosUsuario()
    {
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null)
        {
            String personName = acct.getDisplayName();
            Log.w("USUARIO", personName);
            String personGivenName = acct.getGivenName();
            Log.w("USUARIO", personGivenName);
            String personFamilyName = acct.getFamilyName();
            Log.w("USUARIO", personFamilyName);
            String personEmail = acct.getEmail();
            Log.w("USUARIO", personEmail);
            String personId = acct.getId();
            Log.w("USUARIO", personId);
            Uri personPhoto = acct.getPhotoUrl();
        }
    }

    // --------------------------------------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);

        int id_item;

        id_item = item.getItemId();

        if (id_item == android.R.id.home)
        {
            Intent intent;

            intent = new Intent(this, FilmListFragment.class);
            NavUtils.navigateUpTo(this, intent);

            return true;
        }
        else
        {
            return false;
        }

    }

    // --------------------------------------------------------------

    private void i_mostrarIconoAplicacion()
    {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // --------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        i_mostrarIconoAplicacion();

        Button button_ir_sitio_Web;
        Button button_obtener_soporte;
        Button button_volver;

        i_obtenerDatosUsuario();

        button_ir_sitio_Web = (Button) findViewById(R.id.buttonIrSitioWeb);
        lanzarPaginaWeb(button_ir_sitio_Web);

        button_obtener_soporte = (Button) findViewById(R.id.buttonObtenerSoporte);
        enviarEmail(button_obtener_soporte);

        button_volver = (Button) findViewById(R.id.buttonVolver);
        finalizarAboutActivity(button_volver);

    }
}
