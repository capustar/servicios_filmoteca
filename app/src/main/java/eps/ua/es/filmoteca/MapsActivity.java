package eps.ua.es.filmoteca;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback
{

    private GoogleMap mMap;

    // --------------------------------------------------------------

    private Film i_FilmSeleccionado()
    {
        Intent intent;
        int indice_film_seleccionado;

        intent = getIntent();
        indice_film_seleccionado = intent.getIntExtra(MainActivity.POSICION_FILM_SELECCIONADO_EXTRA, 0);

        return FilmDataSource.getFilm(indice_film_seleccionado);
    }

    // --------------------------------------------------------------

    private void i_anadirMarca()
    {
        // Add a marker in Sydney and move the camera
        Film pelicula;
        LatLng posicion;
        String descripcion;
        MarkerOptions marca;

        pelicula = i_FilmSeleccionado();

        posicion = new LatLng(pelicula.getLatitude(), pelicula.getLongitude());

        descripcion = "Director: " + pelicula.getDirector()
                + "\n" + pelicula.getYear();

        marca = new MarkerOptions()
                .position(posicion)
                .title(pelicula.getTitle())
                .snippet(descripcion);

        mMap.addMarker(marca);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(posicion));
    }

    // --------------------------------------------------------------

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;

        i_anadirMarca();
    }

    // --------------------------------------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);

        int id_item;

        id_item = item.getItemId();

        if (id_item == android.R.id.home)
        {
            Intent intent;

            intent = new Intent(this, FilmEditActivity.class);
            NavUtils.navigateUpTo(this, intent);

            return true;
        }
        else
        {
            return false;
        }

    }

    // --------------------------------------------------------------

    private void i_mostrarIconoAplicacion()
    {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // --------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        i_mostrarIconoAplicacion();
    }
}
