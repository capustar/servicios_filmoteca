package eps.ua.es.filmoteca;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class FilmAdapter extends RecyclerView.Adapter<FilmAdapter.ViewHolder>
{
    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView mimage_icono;
        public TextView mtext_view_titulo;
        public TextView mtext_view_director;

        public ViewHolder(View v)
        {
            super(v);

            mimage_icono = (ImageView)v.findViewById(R.id.imageIcono);
            mtext_view_titulo = (TextView)v.findViewById(R.id.textViewTitulo);
            mtext_view_director = (TextView)v.findViewById(R.id.textViewDirector);
        }

        public void bind(Film f)
        {
            mimage_icono.setImageResource(f.getImageResId());
            mtext_view_titulo.setText(f.getTitle());
            mtext_view_director.setText(f.getDirector());
        }
    }

    private List<Film> mfilms;
    private onItemClickListener mListener;

    public FilmAdapter(List<Film> films)
    {
        mfilms = films;
    }

    public interface onItemClickListener
    {
        public void onItemClick(Film f, int position);
    }

    public void setOnItemListener(onItemClickListener listener)
    {
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_film, parent, false);
        final ViewHolder holder = new ViewHolder(v);

        v.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                int position;

                position = holder.getAdapterPosition();

                if (mListener != null)
                {
                    mListener.onItemClick(mfilms.get(position), position);
                }
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.bind(mfilms.get(position));
    }

    @Override
    public int getItemCount()
    {
        return mfilms.size();
    }
}
