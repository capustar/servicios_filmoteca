package eps.ua.es.filmoteca;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class MainActivity extends AppCompatActivity implements FilmListFragment.OnItemSelectedListener
{
    public static final String POSICION_FILM_SELECCIONADO_EXTRA = "extra_film_seleccionado";

    private AdView mAdView;
    private InterstitialAd interstitialAd;

    // --------------------------------------------------------------

    /*
    private void i_PRUEBAcrearArchivoPeliculas()
    {
        FilmDataSource film_data_source;

        film_data_source = new FilmDataSource();

        //film_data_source.i_crearFicheroDrive(this);
        film_data_source.existeArchivoGoogleDrive(this);
    }
    */

    // --------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //MobileAds.initialize(this, getString(R.string.banner_ad_unit_id));

        if (findViewById(R.id.fragmentContainer) != null)
        {
            if (savedInstanceState != null)
            {
                return;
            }

            FilmListFragment fragment_lista = new FilmListFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragment_lista).commit();

            //i_PRUEBAcrearArchivoPeliculas();
        }
    }

    // --------------------------------------------------------------

    @Override
    public void onItemSelected(int position)
    {
        FilmDataFragment fragment_datos_pelicula = (FilmDataFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentDatosPelicula);

        if (fragment_datos_pelicula != null)
        {
            fragment_datos_pelicula.actualizaDatosVista(position);
        }
        else
        {
            Bundle args = new Bundle();
            FragmentTransaction transaction;

            fragment_datos_pelicula = new FilmDataFragment();

            args.putInt(POSICION_FILM_SELECCIONADO_EXTRA, position);
            fragment_datos_pelicula.setArguments(args);

            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentContainer, fragment_datos_pelicula);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
}
