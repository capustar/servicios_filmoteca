package eps.ua.es.filmoteca;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.*;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static android.app.Activity.RESULT_OK;

public class FilmDataFragment extends Fragment
{
    private static final int CODIGO_ACTIVIDAD_ABRIR_FILM_EDIT = 1;
    private int INDICE_PELICULA_SELECCIONADA = 0;
    private View vista;

    // --------------------------------------------------------------

    private Film i_FilmSeleccionado()
    {
        return FilmDataSource.getFilm(INDICE_PELICULA_SELECCIONADA);
    }

    // --------------------------------------------------------------

    public boolean i_tieneArgumentos()
    {
        if (getArguments() != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // --------------------------------------------------------------

    private void i_abrirUbicacion(Button button, double latitud, double longitud)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent;

                intent = new Intent(getContext(), MapsActivity.class);
                intent.putExtra(MainActivity.POSICION_FILM_SELECCIONADO_EXTRA, INDICE_PELICULA_SELECCIONADA);

                startActivity(intent);
            }
        });
    }

    // --------------------------------------------------------------

    public void rellenaDatosVista()
    {
        if (INDICE_PELICULA_SELECCIONADA >= 0)
        {
            Film film;
            ImageView image_pelicula;
            TextView text_view_nombre_pelicula;
            TextView text_view_nombre_director;
            TextView text_view_ano;
            TextView text_view_genero_formato;
            String genero;
            String formato;
            Button button_ver_imdb;
            Button button_ver_ubicacion;
            TextView text_view_notas_anadidas;

            film = i_FilmSeleccionado();

            image_pelicula = (ImageView)vista.findViewById(R.id.imagePelicula);
            image_pelicula.setImageResource(film.imageResId);

            text_view_nombre_pelicula = (TextView) vista.findViewById(R.id.textViewNombrePelicula);
            text_view_nombre_pelicula.setText(film.title);

            text_view_nombre_director = (TextView)vista.findViewById(R.id.textViewNombreDirector);
            text_view_nombre_director.setText(film.director);

            text_view_ano = (TextView)vista.findViewById(R.id.textViewAno);
            text_view_ano.setText(String.valueOf(film.year));

            text_view_genero_formato = (TextView)vista.findViewById(R.id.textViewGeneroFormato);
            genero = getResources().getStringArray(R.array.Generos)[film.genre];
            formato = getResources().getStringArray(R.array.Formatos)[film.format];
            text_view_genero_formato.setText(genero + ", " + formato);

            button_ver_imdb = (Button)vista.findViewById(R.id.buttonVerIMDB);
            i_abrirEnlaceImdb(button_ver_imdb, film.imdbUrl);

            button_ver_ubicacion = (Button)vista.findViewById(R.id.buttonUbicacion);
            i_abrirUbicacion(button_ver_ubicacion, 0.0, 0.0);

            text_view_notas_anadidas = (TextView)vista.findViewById(R.id.textViewNotasAnadidas);
            text_view_notas_anadidas.setText(film.comments);
        }
    }

    // --------------------------------------------------------------

    public void actualizaDatosVista(int indice_pelicula)
    {
        INDICE_PELICULA_SELECCIONADA = indice_pelicula;
        rellenaDatosVista();
    }

    // --------------------------------------------------------------

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        assert (requestCode == CODIGO_ACTIVIDAD_ABRIR_FILM_EDIT);

        if (resultCode == RESULT_OK)
        {
            Toast.makeText(getContext(), "Los cambios se han guardado con éxito.", Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(getContext(), "No se han producido cambios.", Toast.LENGTH_LONG).show();
        }
    }

    // --------------------------------------------------------------

    private void i_abrirFilmEditActivity(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                if (INDICE_PELICULA_SELECCIONADA >= 0 && FilmDataSource.hayFilms() == true)
                {
                    Intent intent;

                    intent = new Intent(getContext(), FilmEditActivity.class);
                    intent.putExtra(MainActivity.POSICION_FILM_SELECCIONADO_EXTRA, INDICE_PELICULA_SELECCIONADA);
                    startActivityForResult(intent, CODIGO_ACTIVIDAD_ABRIR_FILM_EDIT);
                }
            }
        });
    }

    // --------------------------------------------------------------

    private void abrirFilmListActivity(Button button)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                i_ocultarIconoAplicacion();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    // --------------------------------------------------------------

    private void i_abrirEnlaceImdb(Button button, final String url)
    {
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                if (url != null && !url.isEmpty())
                {
                    try
                    {
                        Intent intent;

                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                    }
                    catch (ActivityNotFoundException e)
                    {
                        Toast toast;

                        toast = Toast.makeText(getContext(), "El enlace no es valido.", Toast.LENGTH_SHORT);

                        toast.show();
                    }
                }
                else
                {
                    Toast toast;

                    toast = Toast.makeText(getContext(), "El enlace esta vacio.", Toast.LENGTH_SHORT);

                    toast.show();
                }
            }
        });
    }

    // --------------------------------------------------------------

    private void i_ocultarIconoAplicacion()
    {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setHasOptionsMenu(false);
    }

    // --------------------------------------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);
        int id_item;

        id_item = item.getItemId();

        if (id_item == android.R.id.home)
        {
            i_ocultarIconoAplicacion();
            getActivity().getSupportFragmentManager().popBackStack();

            return true;
        }
        else
        {
            return false;
        }
    }

    // --------------------------------------------------------------

    private void i_mostrarIconoAplicacion()
    {
        if (i_tieneArgumentos() == true)
        {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setHasOptionsMenu(true);
        }
        else
        {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            setHasOptionsMenu(false);
        }
    }

    // --------------------------------------------------------------

    private void i_calculaIndicePelicula()
    {
        if (i_tieneArgumentos() == true)
            INDICE_PELICULA_SELECCIONADA = getArguments().getInt(MainActivity.POSICION_FILM_SELECCIONADO_EXTRA);
        else
            INDICE_PELICULA_SELECCIONADA = -1;
    }

    // --------------------------------------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        Button button_editar_pelicula;
        Button button_volver_principal;

        vista = inflater.inflate(R.layout.activity_film_data, container, false);

        i_mostrarIconoAplicacion();

        i_calculaIndicePelicula();
        rellenaDatosVista();

        button_editar_pelicula  = (Button) vista.findViewById(R.id.buttonEditarPelicula);
        i_abrirFilmEditActivity(button_editar_pelicula);

        button_volver_principal = (Button) vista.findViewById(R.id.buttonVolverPrincipal);

        if (i_tieneArgumentos() == true)
        {
            abrirFilmListActivity(button_volver_principal);
        }
        else
        {
            button_volver_principal.setVisibility(View.INVISIBLE);
        }

        return vista;
    }

    // --------------------------------------------------------------

    @Override
    public void onStart()
    {
        super.onStart();
        rellenaDatosVista();
    }
}
