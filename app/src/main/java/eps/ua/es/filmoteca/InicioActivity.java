package eps.ua.es.filmoteca;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashSet;
import java.util.Set;

public class InicioActivity extends AppCompatActivity implements View.OnClickListener
{
    private static final int CODIGO_ACTIVITY_SIGN_IN = 1;
    private static final int CODIGO_ACTIVITY_SIGN_IN_DRIVE = 2;

    GoogleSignInClient m_google_sign_in_client;
    private InterstitialAd interstitialAd;

    // --------------------------------------------------------------

    private void i_configurarInicioSesion()
    {
        SignInButton sign_in_button;
        GoogleSignInOptions google_sign_in_options;

        sign_in_button = (SignInButton) findViewById(R.id.ai_signInButton);
        sign_in_button.setOnClickListener(this);

        google_sign_in_options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(Drive.SCOPE_FILE)
                .requestScopes(Drive.SCOPE_APPFOLDER)
                .build();

        m_google_sign_in_client = GoogleSignIn.getClient(this, google_sign_in_options);
    }

    // --------------------------------------------------------------

    private void i_abrirListaPeliculas()
    {
        Intent intent;

        intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    // --------------------------------------------------------------

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask)
    {
        try
        {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            //updateUI(account);

            MobileAds.initialize(this, "ca-app-pub-3940256099942544/1033173712");
            interstitialAd = new InterstitialAd(this);
            interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
            interstitialAd.loadAd(new AdRequest.Builder().build());

            interstitialAd.setAdListener( new AdListener()
            {
                @Override
                public void onAdClosed()
                {
                    super.onAdClosed();
                }

                @Override
                public void onAdFailedToLoad(int i)
                {
                    super.onAdFailedToLoad(i);
                }

                @Override
                public void onAdLeftApplication()
                {
                    super.onAdLeftApplication();
                }

                @Override
                public void onAdOpened()
                {
                    super.onAdOpened();
                }

                @Override
                public void onAdLoaded()
                {
                    super.onAdLoaded();
                    interstitialAd.show();
                }

                @Override
                public void onAdClicked()
                {
                    super.onAdClicked();
                }

                @Override
                public void onAdImpression()
                {
                    super.onAdImpression();
                }
            });

            i_abrirListaPeliculas();
        }
        catch (ApiException e)
        {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("ERROR", "signInResult:failed code=" + e.getStatusCode());
            //updateUI(null);
            Toast.makeText(this, "Ha habido un error en el login.", Toast.LENGTH_LONG).show();
        }
    }

    // --------------------------------------------------------------

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == CODIGO_ACTIVITY_SIGN_IN)
        {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    /*
    // --------------------------------------------------------------

    private void i_PRUEBAcrearArchivoPeliculas()
    {
        FilmDataSource film_data_source;

        film_data_source = new FilmDataSource();

        //film_data_source.i_crearFicheroDrive(this);
        film_data_source.existeArchivoGoogleDrive(this);
    }
    */

    // --------------------------------------------------------------

    private void i_signIn()
    {
        Intent intent;

        intent = m_google_sign_in_client.getSignInIntent();
        startActivityForResult(intent, CODIGO_ACTIVITY_SIGN_IN);
    }

    // --------------------------------------------------------------

    @Override
    protected void onStart()
    {
        super.onStart();

        Set<Scope> required_scopes;
        GoogleSignInAccount google_sign_in_account;

        required_scopes = new HashSet<>(2);
        required_scopes.add(Drive.SCOPE_FILE);
        required_scopes.add(Drive.SCOPE_APPFOLDER);

        google_sign_in_account = GoogleSignIn.getLastSignedInAccount(this);

        if (google_sign_in_account != null && google_sign_in_account.getGrantedScopes().containsAll(required_scopes))
        {
            i_abrirListaPeliculas();
        }
    }

    // --------------------------------------------------------------

    private void i_mostrarToken()
    {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("FirebaseInstanceID", "Token actual: " + refreshedToken);
    }

    // --------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        i_configurarInicioSesion();

        i_mostrarToken();
    }

    // --------------------------------------------------------------
    // View.OnClickListener
    // --------------------------------------------------------------

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.ai_signInButton:
                i_signIn();
                break;

            default:
                break;
        }
    }
}
