package eps.ua.es.filmoteca;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.logging.LogRecord;

/**
 * Created by RosaDiez on 04/04/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService
{
    final private String i_OPERACION_ALTA = "alta";
    final private String i_OPERACION_BAJA = "baja";

    final private String i_KEY_TITLE = "title";
    final private String i_KEY_DIRECTOR = "director";
    final private String i_KEY_YEAR = "year";
    final private String i_KEY_GENRE = "genre";
    final private String i_KEY_FORMAT = "format";
    final private String i_KEY_IMDB = "imdb";
    final private String i_KEY_COMMENTS = "comments";

    private Handler m_handler = new Handler(Looper.getMainLooper())
    {

        @Override
        public void handleMessage(Message message)
        {
            // This is where you do your work in the UI thread.
            // Your worker tells you in the message what to do.
            FilmDataSource film_data_source;

            film_data_source = new FilmDataSource();
            Log.d("FirebaseMessaging", "Creado film data source");

            Film film = (Film) message.obj;

            film_data_source.anadir_nuevo_film(film, null);
            FilmListFragment.adapter.notifyDataSetChanged();
        }
    };

    // --------------------------------------------------------------

    private void i_leerMensaje(String tipo_operacion, Map<String, String> datos)
    {
        if (tipo_operacion.equals(i_OPERACION_ALTA))
        {
            Log.d("FirebaseMessaging", "Tipo operacion: alta");


            Film film;
            String title;
            String director;
            int year;
            int genre;
            int format;
            String imdbUrl;
            String comments;

            title = datos.get(i_KEY_TITLE);
            director = datos.get(i_KEY_DIRECTOR);
            year = Integer.parseInt(datos.get(i_KEY_YEAR));
            genre = Integer.parseInt(datos.get(i_KEY_GENRE));
            format = Integer.parseInt(datos.get(i_KEY_FORMAT));
            imdbUrl = datos.get(i_KEY_IMDB);
            comments = datos.get(i_KEY_COMMENTS);

            Log.d("FirebaseMessaging", "title: " + title);
            Log.d("FirebaseMessaging", "director: " + director);
            Log.d("FirebaseMessaging", "year: " + year);
            Log.d("FirebaseMessaging", "genre: " + genre);
            Log.d("FirebaseMessaging", "format: " + format);
            Log.d("FirebaseMessaging", "imdbUrl: " + imdbUrl);
            Log.d("FirebaseMessaging", "comments: " + comments);

            film = new Film();

            film.setTitle(title);
            film.setDirector(director);
            film.setYear(year);
            film.setGenre(genre);
            film.setFormat(format);
            film.setImdbUrl(imdbUrl);
            film.setComments(comments);
            film.imageResId = R.mipmap.ic_launcher;

            Message completeMessage = m_handler.obtainMessage(1, film);
            completeMessage.sendToTarget();
        }
        else if (tipo_operacion.equals(i_OPERACION_BAJA))
        {

        }


    }

    // --------------------------------------------------------------

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        super.onMessageReceived(remoteMessage);
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("FirebaseMessaging", "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0)
        {
            Log.d("FirebaseMessaging", "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                //scheduleJob();
            } else {
                // Handle message within 10 seconds
                //handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null)
        {
            Log.d("FirebaseMessaging", "Message Notification Body: " + remoteMessage.getNotification().getBody());
            String tipo_operacion;
            Map<String, String> datos;

            tipo_operacion = remoteMessage.getNotification().getBody();
            datos = remoteMessage.getData();

            i_leerMensaje(tipo_operacion, datos);
        }

        if (remoteMessage.getData().size() > 0 && remoteMessage.getNotification() != null)
        {
            Log.d("Entra en el if", "Message Notification Body: " + remoteMessage.getNotification().getBody());


        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
}
