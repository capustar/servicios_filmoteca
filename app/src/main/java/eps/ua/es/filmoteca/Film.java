package eps.ua.es.filmoteca;

import java.io.Serializable;

public class Film implements Serializable
{
    public final static int FORMAT_DVD = 0;
    public final static int FORMAT_BLURAY = 1;
    public final static int FORMAT_DIGITAL = 2;

    public final static int GENRE_ACTION = 0;
    public final static int GENRE_COMEDY = 1;
    public final static int GENRE_DRAMA = 2;
    public final static int GENRE_SCIFI = 3;
    public final static int GENRE_HORROR = 4;

    public int imageResId;
    public String title;
    public String director;
    public int year;
    public double latitude;
    public double longitude;
    public int genre;
    public int format;
    public String imdbUrl;
    public String comments;

    public String toString()
    {
        return title;
    }

    public int getImageResId()
    {
        return imageResId;
    }

    public void setImageResId(int imageResId)
    {
        this.imageResId = imageResId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDirector()
    {
        return director;
    }

    public void setDirector(String director)
    {
        this.director = director;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    public int getGenre()
    {
        return genre;
    }

    public void setGenre(int genre)
    {
        if (genre <= GENRE_HORROR && genre >= GENRE_ACTION)
        {
            this.genre = genre;
        }
        else
        {
            this.genre = GENRE_ACTION;
        }
    }

    public int getFormat()
    {
        return format;
    }

    public void setFormat(int format)
    {
        if (format <= FORMAT_DIGITAL && format >= FORMAT_DVD)
        {
            this.format = format;
        }
        else
        {
            this.format = FORMAT_DVD;
        }
    }

    public String getImdbUrl()
    {
        return imdbUrl;
    }

    public void setImdbUrl(String imdbUrl)
    {
        this.imdbUrl = imdbUrl;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }
}
