package eps.ua.es.filmoteca;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.github.scribejava.apis.TwitterApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;

import org.json.JSONObject;
import org.json.JSONException;

/**
 * Created by RosaDiez on 06/04/2018.
 */

public class Twitter
{

    public static final String TAG = "TwitterDebug";

    private final String i_NOMBRE_PREFERENCIAS = "PreferenciasTwitterbase";

    private final String i_PREFERENCIA_ACCESS_SECRET = "access_Secret";
    private final String i_PREFERENCIA_ACCESS_TOKEN = "access_token";

    // CONSTANTS
    public static final String API_KEY = "N4qP2v227vmTVZdEuys2TgANH";
    public static final String API_SECRET = "GVPnDezortdGU4RgWkpZQ1nStNxUVOcGLhxJ9DWPl273Vl5yct";
    public static final String PROTECTED_RESOURCE_URL = "https://api.twitter.com/1.1/account/verify_credentials.json";
    public static final String PROTECTED_RESOURCE_URL_SEND_TWEET = "https://api.twitter.com/1.1/statuses/update.json";
    public static final String CALLBACKURL = "app://filmoteca";

    private final OAuth10aService service = new ServiceBuilder(API_KEY)
            .apiSecret(API_SECRET)
            .callback(CALLBACKURL)
            .build(TwitterApi.instance());

    private static OAuth1RequestToken requestToken;

    private Context context;

    // --------------------------------------------------------------

    public Twitter(Context context)
    {
        this.context = context;
    }

    // --------------------------------------------------------------

    public boolean estaLogueado()
    {
        boolean logueado;
        SharedPreferences settings = context.getSharedPreferences(i_NOMBRE_PREFERENCIAS, 0);

        logueado = false;

        if (settings.getString(i_PREFERENCIA_ACCESS_TOKEN, null) != null && settings.getString(i_PREFERENCIA_ACCESS_SECRET, null) != null)
        {
            Log.d(TAG, "onResume: accessToken y accessSecret en SharedPreferences");
            // settings have been found make request
            //new getUser().execute();

            logueado = true;
        }
        else
        {
            Log.d(TAG, "onResume: accessToken y accessSecret no estan en SharedPreferences");

            logueado = false;
        }

        return logueado;
    }

    // --------------------------------------------------------------

    public void cerrarSesion()
    {
        SharedPreferences settings = context.getSharedPreferences(i_NOMBRE_PREFERENCIAS, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(i_PREFERENCIA_ACCESS_TOKEN);
        editor.remove(i_PREFERENCIA_ACCESS_SECRET);
        editor.apply();
    }

    // --------------------------------------------------------------

    public String getAuthUrl()
    {
        String authUrl = null;

        try
        {
            // 1
            authUrl = new authUrl().execute().get();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return authUrl;
    }

    // --------------------------------------------------------------

    public void oauthEnd(String verifier)
    {
        new OauthEnd().execute(verifier);
    }

    // --------------------------------------------------------------

    public void getUser()
    {
        new getUser().execute();
    }

    // --------------------------------------------------------------

    public void sendTweet(String titulo_pelicula)
    {
        new sendTweet().execute(titulo_pelicula);
    }

    // --------------------------------------------------------------
    // **************************************************************
    // --------------------------------------------------------------

    private class getUser extends AsyncTask<String, Void, String>
    {

        // --------------------------------------------------------------

        @Override
        protected String doInBackground(String... params)
        {
            SharedPreferences settings = context.getSharedPreferences(i_NOMBRE_PREFERENCIAS, 0);
            String respuesta = null;

            Log.d(TAG, "getUser: Hacemos una petición de ejemplo con el Token de acceso que tenemos");
            String access_token = settings.getString(i_PREFERENCIA_ACCESS_TOKEN, null);
            String access_secret = settings.getString(i_PREFERENCIA_ACCESS_SECRET, null);
            OAuth1AccessToken newAccessToken = new OAuth1AccessToken(access_token, access_secret);
            final OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
            service.signRequest(newAccessToken, request);

            try
            {
                final Response response = service.execute(request);
                if (response.isSuccessful())
                {
                    Log.d(TAG, "La respuesta a la solicitud es satisfactoria");
                    respuesta = response.getBody();
                }
                else
                {
                    Log.d(TAG, "La respuesta a la solicitud NO es satisfactoria");
                    respuesta = "-1";
                }
            }
            catch (Exception e)
            {
                Log.d(TAG, "Excepción producida al ejecutar la solicitud de acceso al recurso protegido");
            }

            return respuesta;
        }

        // --------------------------------------------------------------

        @Override
        protected void onPostExecute(String result)
        {
            try
            {
                JSONObject myJson = new JSONObject(result);
                String name = myJson.optString("name");

                // set info
                SharedPreferences settings = context.getSharedPreferences(i_NOMBRE_PREFERENCIAS, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("name", name);
                editor.apply();

                Log.d(TAG, "Nombre del usuario: " + name);

            }
            catch (JSONException e)
            {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    // --------------------------------------------------------------
    // **************************************************************
    // --------------------------------------------------------------

    private class sendTweet extends AsyncTask<String, Void, String>
    {
        // --------------------------------------------------------------

        private String i_textoTweet(String titulo_pelicula)
        {
            String texto;
            int numero_caracteres_max;

            texto = "Me ha gustado " + titulo_pelicula;

            numero_caracteres_max = 240;
            if (texto.length() > numero_caracteres_max)
            {
                String puntos_suspensivos;
                String texto_adaptado;

                puntos_suspensivos = "...";

                texto_adaptado = texto.substring(0, 238) + puntos_suspensivos;

                texto = texto_adaptado;
            }

            return texto;
        }

        // --------------------------------------------------------------

        @Override
        protected String doInBackground(String... params)
        {
            SharedPreferences settings = context.getSharedPreferences(i_NOMBRE_PREFERENCIAS, 0);
            String respuesta = null;
            String titulo_pelicula = params[0];
            String texto_tweet;

            Log.d(TAG, "getUser: Enviamos el tweet con el Token de acceso que tenemos");
            String access_token = settings.getString(i_PREFERENCIA_ACCESS_TOKEN, null);
            String access_secret = settings.getString(i_PREFERENCIA_ACCESS_SECRET, null);
            Log.d(TAG, "access_token: " + access_token);
            Log.d(TAG, "access_secret: " + access_secret);
            OAuth1AccessToken newAccessToken = new OAuth1AccessToken(access_token, access_secret);
            final OAuthRequest request = new OAuthRequest(Verb.POST, PROTECTED_RESOURCE_URL_SEND_TWEET);

            //request.addBodyParameter("status", "Tweet de prueba");

            texto_tweet = i_textoTweet(titulo_pelicula);
            request.addQuerystringParameter("status", texto_tweet);

            service.signRequest(newAccessToken, request);

            try
            {
                final Response response = service.execute(request);

                if (response.isSuccessful())
                {
                    Log.d(TAG, "La respuesta a la solicitud es satisfactoria");

                    respuesta = response.getBody();
                }
                else
                {
                    Log.d(TAG, "La respuesta a la solicitud NO es satisfactoria");
                    Log.d(TAG, response.getBody());
                    respuesta = "-1";
                }
            }
            catch (Exception e)
            {
                Log.d(TAG, "Excepción producida al ejecutar la solicitud de acceso al recurso protegido");
            }

            return respuesta;
        }

        // --------------------------------------------------------------

        @Override
        protected void onPostExecute(String result)
        {
            Toast toast;

            if (result.equals("-1") == true)
            {
                toast = Toast.makeText(context, "Ha habido un problema marcandola como favorita.", Toast.LENGTH_SHORT);
            }
            else
            {
                toast = Toast.makeText(context, "Pelicula marcada como favorita.", Toast.LENGTH_SHORT);
            }

            toast.show();
        }
    }

    // --------------------------------------------------------------
    // **************************************************************
    // --------------------------------------------------------------

    private class authUrl extends AsyncTask<String, Void, String>
    {

        // --------------------------------------------------------------

        @Override
        protected String doInBackground(String... params)
        {
            try
            {
                Log.d(TAG, "authUrl: Solicitamos las credenciales temporales");
                requestToken = service.getRequestToken();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            Log.d(TAG, "authUrl: Componemos la URL a la que mandar al usuario para que autorize el acceso");
            return service.getAuthorizationUrl(requestToken);
        }

    }

    // --------------------------------------------------------------
    // **************************************************************
    // --------------------------------------------------------------

    private class OauthEnd extends AsyncTask<String, Void, Void>
    {

        // --------------------------------------------------------------

        @Override
        protected Void doInBackground(String... params)
        {
            SharedPreferences settings = context.getSharedPreferences(i_NOMBRE_PREFERENCIAS, 0);
            final SharedPreferences.Editor editor = settings.edit();

            final String verifier = params[0];
            Log.d(TAG, "OauthEnd: Una vez con el verificador, solicitamos el Token de acceso");
            try
            {
                // Setup storage for access token
                final OAuth1AccessToken accessToken = service.getAccessToken(requestToken, verifier);

                Log.d(TAG, "OauthEnd: Deveulto el Token de acceso, lo guardamos en las SharedPreferences para tenerlo disponible para hacer peticiones");
                editor.putString(i_PREFERENCIA_ACCESS_TOKEN, accessToken.getToken());
                editor.putString(i_PREFERENCIA_ACCESS_SECRET, accessToken.getTokenSecret());

                editor.apply();
            }
            catch (Exception e)
            {
                Log.d(TAG, "Excepción producidad al intentar obtener el accessToken");
            }
            return null;
        }
    }
}
